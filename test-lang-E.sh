#!/bin/bash

# Measure the last 20 commits to match Ekstazi benchmark.

interrupted()
{
    exit $?
}

trap interrupted SIGINT

set -eu

export PROJECT="lang"

source java-config.sh

JAVA_HOME="${JAVA_SE7_HOME}"
JAVA="${JAVA_HOME}/bin/java"

./update-testlib.sh
./clean-project.sh

set +e

run_tests()
{
	# Checkout the commit.
	./checkout-${PROJECT}.sh $1

	# Run test selector.
	DV="../lib/testtool.jar"
	(cd "$PROJECT"; "$JAVA" -DlogLevel=INFO -jar "$DV" compile -project . >> log.tmp 2>&1)
	if [ "$?" -eq "0" ]; then
		(cd "$PROJECT"; "$JAVA" -DlogLevel=INFO -jar "$DV" runtests -project . 2>&1 | tee -a log.tmp | grep "Running tests:")
		(cd "$PROJECT"; "$JAVA" -jar "$DV" runall -project . >> log.tmp 2>&1)
	fi
}

export TEST_JAVA_HOME="${JAVA_SE7_HOME}"

# Commit -21: a52b0d02fc72e693003068cf537a0940ac90643a
#run_tests 159622a6a1ea16a9e1dffaa0cf133d0fd1256c0f
#run_tests 3a71c03cc8baab764300512ee890a490d8963d80
#run_tests debe634ed0abe6536e776ca27d110e29965a5963
#run_tests f5c4730e5ec97167cc3bc8f2209254f85a97a598
#run_tests e8a61a8f01dfac441a085213043d429e3d3de1db
#run_tests 913fb02282bd215f4f0f254b9659c75fe6dd5457
#run_tests 144032cc065648a0d515fa36b95bea6f473a9ccd
#run_tests 831b0ed52cfc4a4921cb995c2e4c550f314f6752
#run_tests 9333c2ccb296febd42edc7b91cd4f6f0cf164ebc
#run_tests 61b5d26c1a3c8268f46e62e086ea9a8e0e23a555
#run_tests 6496decede28407a674fad09257dacdf9270c775
#run_tests 266b9b834b2b44c77fb9886938ce99bb1c148858
#run_tests 61f5ee1a4150072f8d55faf1a47d0a78370882dd
#run_tests 5fa0cd22b61926fc1b27079f6a117d7d1cdd5c5c
#run_tests 2eff076831c3d8b015e9959fde6e90a6f9939244
#run_tests 0d9d57dda8a7bf989aaad315e93312ee30df48bb
#run_tests 2e5ac8cbf238040ed775a792576a3f0e7991cdd7
#run_tests 4b00ec5a8e8bd9961178b7f21556dda7f0bb6eed
#run_tests 8795162af1e7ad348b154e0069a127730b216450
#run_tests 0d9b8cb7a7cc5a9ebd3c08029f2d53fa9bcd9a70

END_COMMIT="0d9b8cb7a7cc5a9ebd3c08029f2d53fa9bcd9a70"

# Run 20 commits with the last one being END_COMMIT:
for COMMIT in $(cd "${PROJECT}"; git rev-list --reverse "${END_COMMIT}" | tail -n20); do
	run_tests "${COMMIT}"
done


TIMESTAMP=$(date "+%Y%m%d-%H%M%S")
cp "${PROJECT}/log.tmp" "logE-${PROJECT}.${TIMESTAMP}"
gzip "logE-${PROJECT}.${TIMESTAMP}"
