#!/bin/bash

# Measure the last 20 commits N number of times.

# Number of measurement series:
N=20

interrupted()
{
    exit $?
}

trap interrupted SIGINT

set -eu

export PROJECT="functor"

source java-config.sh

JAVA_HOME="${JAVA_SE8_HOME}"
JAVA="${JAVA_HOME}/bin/java"
export TEST_JAVA_HOME="${JAVA_SE8_HOME}"

./update-testlib.sh
./clean-project.sh

set +e

run_tests()
{
	# Checkout the commit.
	./checkout-${PROJECT}.sh $1

	# Run the test selector.
	DV="../lib/testtool.jar"
	(cd "$PROJECT"; "$JAVA" -DlogLevel=INFO -jar "$DV" compile -project . >> log.tmp 2>&1)
	if [ "$?" -eq "0" ]; then
		(cd "$PROJECT"; "$JAVA" -DlogLevel=INFO -jar "$DV" runtests -project . 2>&1 | tee -a log.tmp | grep "Running tests:")
		(cd "$PROJECT"; "$JAVA" -jar "$DV" runall -project . >> log.tmp 2>&1)
	fi
	# Backup map for comparison.
	#cp "${PROJECT}/dependencies.map" "${PROJECT}/${1}.map"
}

set +u
if [ ! -z "$1" ]; then
	for arg in "$@"; do
		run_tests "$arg"
	done
	exit 0
fi
set -u

END_COMMIT="3da1a4b1214428c04ca9102ef4567ba61b0ff5be"

# Run N commits with the last one being END_COMMIT:
for COMMIT in $(cd "${PROJECT}"; git rev-list --reverse "${END_COMMIT}" | tail -n1000); do
	run_tests "${COMMIT}"
done

TIMESTAMP=$(date "+%Y%m%d-%H%M%S")
cp "${PROJECT}/log.tmp" "log-${PROJECT}.${TIMESTAMP}"
gzip "log-${PROJECT}.${TIMESTAMP}"

