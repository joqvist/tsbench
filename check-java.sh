#!/bin/bash

interrupted()
{
    exit $?
}

trap interrupted SIGINT

source java-config.sh

echo "Java 4"
${JAVA_SE4_HOME}/bin/java -version
echo "Java 7"
${JAVA_SE7_HOME}/bin/java -version
