#!/bin/bash

interrupted()
{
    exit $?
}

trap interrupted SIGINT

set -eu

export PROJECT="functor"

source java-config.sh

JAVA_HOME="${JAVA_SE8_HOME}"
JAVA="${JAVA_HOME}/bin/java"
export TEST_JAVA_HOME="${JAVA_SE8_HOME}"

./update-testlib.sh
./clean-project.sh

set +e

run_tests()
{
	# Checkout the commit.
	./checkout-${PROJECT}.sh $1

	# Run the test selector.
	DV="../lib/testtool.jar"
	(cd "$PROJECT"; "$JAVA" -DlogLevel=INFO -jar "$DV" compile -project . >> log.tmp 2>&1)
	if [ "$?" -eq "0" ]; then
		(cd "$PROJECT"; "$JAVA" -DlogLevel=INFO -jar "$DV" runtests -project . 2>&1 | tee -a log.tmp | grep "Running tests:")
		(cd "$PROJECT"; "$JAVA" -jar "$DV" runall -project . >> log.tmp 2>&1)
	fi
	# Backup map for comparison.
	#cp "${PROJECT}/dependencies.map" "${PROJECT}/${1}.map"
}

if [ ! -z "$1" ]; then
	for arg in "$@"; do
		run_tests "$arg"
	done
	exit 0
fi

# Commit -21: c42b63b5d018d89035a45f31a91ca05ea648aab3
#run_tests 2d264c2bfbb85b46d6727f95a337c8f8c3050280
#run_tests e586e23b887345a9aeeb0da0d3e726013269a1be
#run_tests 3cae3067fb9394f1788060d834ba998ffc27f422
#run_tests aca36ae7c419e55366ba03c8ef57acd50e8ba90a
#run_tests 7a270450bb3dc169310099f4d04cd322c67d7821
#run_tests fda81a54357390042d7fb4131f5a2b0d76be8376
#run_tests 4c73da5e2272702521bb61e5b4a94c3e8f4547f9
#run_tests d6beb0438ed3432cd4246a62b821735540b45431
#run_tests 56035111e4879168106d68786042538462220765
#run_tests 10c78767a3cc6937733c3a85ea20b6835c5d9bfc
#run_tests 0bb123752e2f7c225a5322d9f00cfd6df1a75510
#run_tests 64dfb78b1f5d5fd7cfcd8c8c5c3b0f58eba09d0a
#run_tests 1eec0a2f3ad3180173478a54b8ca12bca459a844
#run_tests 1004abda7fe5638860661f4cbe81be2e4bddf90c
#run_tests 5db233e4c3e2e64bb39b07fa501943839b875d3f
#run_tests a4c8160726a1abb87560c568861719dfe3698379
#run_tests 9935540e7415f2c78e512eba79e79af92fd79b2f
#run_tests 836eea72d53575a750d3ae56566b895df9eebbb2
#run_tests 38720128bc84de1535a7844f106f7abf8e2576e1
#run_tests 2941c23f509d3a306d6f22aa8ebf912c70b66528

END_COMMIT="2941c23f509d3a306d6f22aa8ebf912c70b66528"

# Run 20 commits with the last one being END_COMMIT:
for COMMIT in $(cd "${PROJECT}"; git rev-list --reverse "${END_COMMIT}" | tail -n20); do
	run_tests "${COMMIT}"
done

TIMESTAMP=$(date "+%Y%m%d-%H%M%S")
cp "${PROJECT}/log.tmp" "logE-${PROJECT}.${TIMESTAMP}"
gzip "logE-${PROJECT}.${TIMESTAMP}"
