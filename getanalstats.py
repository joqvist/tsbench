#!/usr/bin/env python
import sys
import math
import os
import re

class Commit:

	def __init__(self, index, ref):
		self.index = index
		self.ref = ref
		self.parseTime = 0
		self.javaFiles = 0
		self.modCheckTime = 0
		self.javacTime = 0
		self.totalTime = 0
		self.compileError = False

def parselog(logfile):
	refs = [ ]

	checkout = re.compile(r'Checkout ([a-zA-Z0-9]+)')
	allTests = re.compile(r'Running all tests')
	compileError = re.compile(r'Can not run tests; compile error exists!')
	javaFiles = re.compile(r'Parsed (\d+) [jJ]ava files')
	parseTime = re.compile(r'Parsing took (\d+) millis')
	modCheckTime = re.compile(r'Modification checking took (\d+) millis')
	totalTime = re.compile(r'Dependency refresh took (\d+) millis')
	javacTime = re.compile(r'javac compilation took (\d+) millis')

	commit = None
	index = 0

	runningAll = False
	for line in logfile.readlines():
		if checkout.match(line):
			ref = checkout.match(line).group(1)
			commit = Commit(index, ref)
			refs.append(commit)
			index = index + 1
			runningAll = False
		elif javacTime.match(line):
			if not runningAll:
				commit.javacTime = int(javacTime.match(line).group(1)) / 1000.0
		elif allTests.match(line):
			runningAll = True
		elif compileError.match(line):
			commit.compileError = True
		elif javacTime.match(line):
			commit.javacTime = int(javacTime.match(line).group(1)) / 1000.0
		elif modCheckTime.match(line):
			commit.modCheckTime = int(modCheckTime.match(line).group(1)) / 1000.0
		elif parseTime.match(line):
			commit.parseTime = int(parseTime.match(line).group(1)) / 1000.0
		elif totalTime.match(line):
			commit.totalTime = int(totalTime.match(line).group(1)) / 1000.0
		elif javaFiles.match(line):
			commit.javaFiles = int(javaFiles.match(line).group(1))

	return refs

def printresults(refs):
	print("commit"),
	print("javac"),
	print("error"),
	print("files"),
	print("parse"),
	print("modcheck"),
	print("total")
	for commit in sorted(refs, key=lambda c: c.index):
		print(commit.ref),
		print(commit.javacTime),
		if not commit.compileError:
			print("FALSE"),
			print(commit.javaFiles),
			print(commit.parseTime),
			print(commit.modCheckTime),
			print(commit.totalTime)
		else:
			print("TRUE"),
			print("NA"),
			print("NA"),
			print("NA"),
			print("NA")

def main():
	if len(sys.argv) > 1:
		with open(sys.argv[1]) as logfile:
			refs = parselog(logfile)
	else:
		refs = parselog(sys.stdin)

	printresults(refs)

if __name__ == "__main__":
	main()
