#!/bin/bash

# Measure the last 40 commits N number of times.

# Number of measurement series:
N=20

interrupted()
{
    exit $?
}

trap interrupted SIGINT

set -eu

export PROJECT="junit"

source java-config.sh

JAVA_HOME="${JAVA_SE7_HOME}"
JAVA="${JAVA_HOME}/bin/java"

./update-testlib.sh
./clean-project.sh

set +e

run_tests()
{
	# checkout the commit
	./checkout-${PROJECT}.sh $1

	# run test selector
	DV="../lib/testtool.jar"
	(cd "$PROJECT"; "$JAVA" -DlogLevel=INFO -jar "$DV" compile -project . >> log.tmp 2>&1)
	if [ "$?" -eq "0" ]; then
		(cd "$PROJECT"; "$JAVA" -DlogLevel=INFO -jar "$DV" runtests -project . 2>&1 | tee -a log.tmp | grep "Running tests:")
		(cd "$PROJECT"; "$JAVA" -jar "$DV" runall -project . >> log.tmp 2>&1)
	fi
}

export TEST_JAVA_HOME="${JAVA_SE7_HOME}"
export JUNIT_HARNESS_VERSION="JUNIT4"

for i in `seq 1 $N`; do
	if [ -e "${PROJECT}/dependencies.map" ]; then
		rm "${PROJECT}/dependencies.map"
	fi

	echo "Starting ${PROJECT} series $i"

	# first commit is run and discarded (need starting point for measurement
	run_tests 5e2616aa6280909649185240e0194fd14c5147a8
	run_tests 304a03ec5e57f52d9c5490a7de599629f2de3cf7
	run_tests c508fc4443a7e12cf1c9e4e669eebeb12b16b56f
	run_tests 417add342b2161e592707c61f66efc8b9d8feccd
	run_tests 77450c71a7024d85012fa371dbfe98b651353b02
	run_tests 7216e4bd9eceea0be4813da6520a6fa4ed2e3a2a
	run_tests c391c25b230e880289484202f3dc3aa81c373a1c
	run_tests 495a9a93e4a1157e707a01467e7cc3f600c88d19
	run_tests e94c54327486637afea3a3e676def0ce080fb375
	run_tests 60aaf96322abf2071e4f93b2748723a56c5ee668
	run_tests d712ef951802074695c79740f9bb6954b12ee4ce
	run_tests 1fc1c12289c84d19f46d202b98d50bc80b38ab2e
	run_tests 96ca415d66bb37d6d3c54505cf34ce5dce3fecf5
	run_tests bd5b90f75908b7a3ed074a7be29823170f008fb4
	run_tests 4b79be8b3a3d6afdf5e79c89c4a83077b86d5929
	run_tests 39d3fd65ab7d3689486427a1cc0ad0cf22dbb6ec
	run_tests b3a5b536d7a3889b33f956b463351edfbc06b80f
	run_tests 2b6109612b4a70cca1175cb01b0322593647f218
	run_tests aacc4438a98ff483b210862b095fbe9e68f30c0d
	run_tests d462bb939b13b0a95a31258eb872a5295d11df76
	run_tests 207b0d727d365e90a3f03102c7b483492f5f7154
	run_tests 3cea4a9e8f196616ceb9f0bdb1755dace92befd2
	run_tests ac655968cf324d1ccb7628f92c5facc9430f177e
	run_tests e6f04890312dccafb295fb0a38f441c81c9f137f
	run_tests 430e3669badc46ae9ba97e513ddbd1a88a5b7f14
	run_tests d477277831290221d5e557eb0afe02c483289939
	run_tests 3d535e08126bc515ddcb23e845f5fed121faa470
	run_tests dfbf2ae868049c041c926f2bf2d7633056af5820
	run_tests 340beca75040da67e495284ce34756c9f7e567b4
	run_tests 23684f6b47da555ec0cac883776f8cdeef82842a
	run_tests 64a1c76037475cca448ba16bca90a83581c4ef01
	run_tests 3f0adeaf0a812cf88e76527b95c499e3ba99404e
	run_tests de9269563b6af4ccf9e3d3750864158a27eee6d3
	run_tests 54b7613484be714a769a8d62f1ac507912e61a01
	run_tests c7300033b5622aa02a58d4570e598697e58b0b4d
	run_tests 0804ef4cd09b0e94783a9882c60914e22fca5108
	run_tests 0f0152a8cd6dcbdb9f9cbb6ec5ca1cf2629dc12a
	run_tests a90b496a6595856066504baf4f737fb853a6e45d
	run_tests 38010188e5c5e34da29d063e6c002a173f56a539
	run_tests 9c337dcbb8c71cfcbd283a4f481c2794c13465a6
	run_tests 187c56489189d2a6e928fea3d0942e1a5346c517
done

TIMESTAMP=$(date "+%Y%m%d-%H%M%S")
cp "${PROJECT}/log.tmp" "logN-${PROJECT}.${TIMESTAMP}"
gzip "logN-${PROJECT}.${TIMESTAMP}"

