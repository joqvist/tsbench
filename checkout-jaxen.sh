#!/bin/bash

if [ $# -lt "1" ]; then
    echo "Usage: checkout GITHASH"
    exit 1
fi

if [ ! -d "${PROJECT}" ]; then
	echo "Error: no such directory: ${PROJECT}"
	echo "Make sure that the clean-project script is run first!"
	exit 1
fi

cd "${PROJECT}"
echo "Checkout $1" | tee -a log.tmp
git reset --hard $1 > /dev/null
if [ "$?" -ne "0" ]; then
	exit $?
fi

LIB='lib'
if [ -d "$LIB" ]; then
	CLASSPATH=$(JARS=("$LIB"/*.jar); IFS=:; echo "${JARS[*]}")
else
	MVN='mvn'
	if [ ! -d "$MVN" ]; then
		mkdir "$MVN"
		java -jar ../lib/ivy-2.4.0-rc1.jar -dependency jaxen jaxen 1.1.6 -retrieve "$MVN/[artifact]-[revision](-[classifier]).[ext]"
		#java -jar ../lib/ivy-2.4.0-rc1.jar -dependency junit junit 3.8.2 -retrieve "$MVN/[artifact]-[revision](-[classifier]).[ext]"
		#java -jar ../lib/ivy-2.4.0-rc1.jar -dependency junit junit 3.5 -retrieve "$MVN/[artifact]-[revision](-[classifier]).[ext]"
		#cp ../lib/junit-3.5.jar $MVN
		#cp ../lib/junit-3.7.jar $MVN
		#cp ../lib/junit-3.8.jar $MVN
		cp ../lib/junit-3.8.2.jar $MVN
		rm $MVN/jaxen-*
	fi
	# get maven dependencies
	CLASSPATH=$(JARS=("$MVN"/*.jar); IFS=:; echo "${JARS[*]}")
fi

echo "classpath.src=src/java/main\:src/java/test" > dependencies.cfg
echo "classpath.lib=${CLASSPATH//:/\\:}" >> dependencies.cfg
echo "classpath.con=" >> dependencies.cfg
echo "java.home=${TEST_JAVA_HOME}" >> dependencies.cfg
echo "bootclasspath=${TEST_JAVA_HOME}/lib/rt.jar" >> dependencies.cfg
echo "testRunner=JUNIT3" >> dependencies.cfg

