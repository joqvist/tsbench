#!/bin/bash

set -eu

if [ $# -lt "1" ]; then
    echo "Usage: checkout GITHASH"
    exit 1
fi

if [ ! -d "$PROJECT" ]; then
	echo "Error: no such directory: $PROJECT"
	echo "Make sure that the clean-project script is run first!"
	exit 1
fi

cd "$PROJECT"
echo "Checkout $1" | tee -a log.tmp
git reset --hard $1 > /dev/null
if [ "$?" -ne "0" ]; then
	exit $?
fi

LIB='mvnlibs'
if [ -d "$LIB" ]; then
	CLASSPATH=$(JARS=("$LIB"/*.jar); IFS=:; echo "${JARS[*]}")
else
	# Download maven dependencies.
	mkdir "${LIB}"
	mvn org.apache.maven.plugins:maven-dependency-plugin:2.1:copy-dependencies -DoutputDirectory="$(readlink -f ${LIB})"

	# Remove libs that cause build problems.
	rm "${LIB}/guava-20.0-SNAPSHOT.jar"
	rm "${LIB}/guava-gwt-20.0-SNAPSHOT.jar"

	# Build classpath.
	CLASSPATH=$(JARS=("$LIB"/*.jar); IFS=:; echo "${JARS[*]}")
fi

# Generate sources.

# Build test selector configuration:
echo "classpath.src=src\:test\:gen" > dependencies.cfg
echo "classpath.lib=${CLASSPATH//:/\\:}" >> dependencies.cfg
echo "classpath.con=" >> dependencies.cfg
echo "exclude=.+/debugger/.+,.+/gwt/.+,.+/webservice/.+" >> dependencies.cfg
echo "java.home=${TEST_JAVA_HOME}" >> dependencies.cfg
echo "bootclasspath=${TEST_JAVA_HOME}/lib/rt.jar" >> dependencies.cfg
echo "testRunner=JUNIT4" >> dependencies.cfg

