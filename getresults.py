#!/usr/bin/env python
import sys
import math
import os
import re

class Commit:

	def __init__(self, index, ref):
		self.index = index
		self.ref = ref
		self.testTime = 0
		self.fullTestTime = 0
		self.analTime = 0
		self.selectTime = 0
		self.tests = 0
		self.selectedTests = 0
		self.compileError = False
		self.testErrors = 0
		self.modifiedFiles = 0
		self.javacTime = 0

def parselog(logfile):
	refs = [ ]

	checkout = re.compile(r'Checkout ([a-zA-Z0-9]+)')
	testStart = re.compile(r'Running tests: (\d+) (\d+)')
	allTests = re.compile(r'Running all tests')
	noTestsRun = re.compile(r'No tests needed to be run out of (\d+) available tests')
	testEnd = re.compile(r'Test run completed \(took ([\d.]+)s\)')
	testError = re.compile(r'testError: (.+)')
	compileError = re.compile(r'Can not run tests; compile error exists!')
	fileModified = re.compile(r'File (modified|added|deleted): (.+)')
	refreshFinished = re.compile(r'Dependency refresh took (\d+) millis')
	javacTime = re.compile(r'javac compilation took (\d+) millis')
	selectTime = re.compile(r'Test selection took (\d+) millis')

	commit = None
	index = 0

	runningAll = False
	for line in logfile.readlines():
		if checkout.match(line):
			ref = checkout.match(line).group(1)
			commit = Commit(index, ref)
			refs.append(commit)
			index = index + 1
			runningAll = False
		elif testStart.match(line):
			if not runningAll:
				match = testStart.match(line)
				commit.tests = int(match.group(1))
				commit.selectedTests = int(match.group(2))
		elif javacTime.match(line):
			if not runningAll:
				commit.javacTime = int(javacTime.match(line).group(1)) / 1000.0
		elif allTests.match(line):
			runningAll = True
		elif testEnd.match(line):
			time = testEnd.match(line).group(1)
			if runningAll:
				commit.fullTestTime = time
			else:
				commit.testTime = time
		elif testError.match(line):
			commit.testErrors = commit.testErrors + 1
		elif compileError.match(line):
			commit.compileError = True
		elif fileModified.match(line):
			filename = fileModified.match(line).group(2)
			if filename.endswith(".java"):
				commit.modifiedFiles = commit.modifiedFiles + 1
		elif noTestsRun.match(line):
			commit.tests = int(noTestsRun.match(line).group(1))
			commit.selectedTests = 0
		elif refreshFinished.match(line):
			if not runningAll:
				commit.analTime = int(refreshFinished.match(line).group(1)) / 1000.0
		elif selectTime.match(line):
			if not runningAll:
				commit.selectTime = int(selectTime.match(line).group(1)) / 1000.0

	return refs

def printresults(refs):
	print("commit"),
	print("tests"),
	print("selectedTests"),
	print("testTime"),
	print("fullTestTime"),
	print("analTime"),
	print("selectTime"),
	print("compileError"),
	print("testErrors"),
	print("modifiedFiles"),
	print("javacTime")
	for commit in sorted(refs, key=lambda c: c.index):
		print(commit.ref),
		if not commit.compileError:
			print(commit.tests),
			print(commit.selectedTests),
			print(commit.testTime),
			print(commit.fullTestTime),
			print(commit.analTime + commit.selectTime),
			print(commit.selectTime),
			print("FALSE"),
		else:
			print("NA"),
			print("NA"),
			print("NA"),
			print("NA"),
			print("NA"),
			print("NA"),
			print("TRUE"),
		print(commit.testErrors),
		print(commit.modifiedFiles),
		print(commit.javacTime)


def main():
	if len(sys.argv) > 1:
		with open(sys.argv[1]) as logfile:
			refs = parselog(logfile)
	else:
		refs = parselog(sys.stdin)

	printresults(refs)

if __name__ == "__main__":
	main()
