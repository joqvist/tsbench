# AutoRTS Benchmark

This repository contains the unfinished artifact for our paper
"Extraction-Based Regression Test Selection". We submitted the paper to
a conference that did not do artifact evaluation, so the artifact was never fully
completed and may be a bit tricky to use.


## 1. Requirements

This should be built on a Linux or OSX machine with a Bash terminal.
Building on Windows may work.

Additional requirements:

* Bash (to run benchmark scripts)
* R (optional, used to generate statistics & graphs)
* Python (optional, used to generate statistics & graphs)
* Git
* Apache Maven 3.0
* Java 4 JDK
* Java 6 JDK
* Java 7 JDK
* Java 8 JDK

The Java JDK installations are needed for building the benchmark projects.
Some commits can only be compiled Java 4 for example. If a required Java
version is missing or if the path is not correct, then the building of the
commits using that Java version will fail, however the rest of the commits
should be able to build correctly provided the Java version needed by those
commits is available.  To ensure repeatable results it is important to use the
same Java version. Here are the Java versions used in our Regression Test
Selection article:

* Java 4: 1.4.2_19
* Java 6: 1.6.0_45
* Java 7: 1.7.0_45
* Java 8: 1.8.0_71

Java 8 should be used as the default JDK. Please configure the paths to all
other JDKs in the file java-config.sh. Example configurations are provided in
that file.

R libraries needed:

* ggplot2
* plyr
* reshape

Install the R libraries from the R shell using these commands:

    R
    > install.packages("ggplot2", "plyr", "reshape")
	> quit()
	Save workspace image? [y/n/c]: n


## 2. Building

To build AutoRTS, run the following command in a terminal emulator on Linux/OSX:

    git submodule init
    git submodule update # Fetches the autorts submodule.
    cd autorts
    git submodule init
    git submodule update # Fetches the autorts/extendj submodule.
    cd .. # Back to TSBench root directory.
    bash gradlew -pautorts


A few warnings are expected, and at the end Gradle should say "BUILD
SUCCESSFUL" if everything built correctly.  Gradle has then built the AutoRTS
library launcher.jar in the ./autorts/core/build/libs directory. This library
file is used for benchmarking and copied to lib/testtool.jar by the
update-testlib.sh script.

## 3. Benchmarking

All benchmarking is done with a series of Bash scripts:

* test-{project}.sh - runs RTS on most commits in the history of the given
  project. Some commits are excluded due to build problems.
* test-{project}-N.sh - runs the last 40 commits of the given project 20
  times.
* test-{project}-E.sh - runs the last 20 commits of the given project 20
  times.

The existing configured benchmark projects are:

* jaxen
* lang
* junit
* closure (only test-E benchmarking)
* functor (only test-E benchmarking)

Before running a benchmark you must configure your JDK installation paths as
described in Requirements. You will also need to modify the execute permission
of all the Bash scripts for example using the command

    chmod u+x *.sh


Now everything should be set to run a benchmark. For example to run jaxen-N:

    ./test-jaxen-N.sh


The benchmark scripts typically take 2-8 hours to run, so a variant of this
script that only runs the latest 10 commits once can be used to check that the
benchmark setup works. This script uses Java 7 to build the last 10 commits:

    ./test-jaxen-10.sh


You should see output similar to this while a benchmark script is running:

    Checkout 8243e63340c3ba9439aec2196cf0772ca28da449
    Running tests: 444 421
    Checkout b3197dcbd525da04c136d6a9908ea7ec5196d9ea
    Checkout 36f35a1e2372e2bea7a9edc17c86cf895e17dc2f
    Running tests: 444 19
    Checkout 97c2ca38c77af90b877c698e7c0738ac637e11b5
    Running tests: 445 391
    Checkout d44930aa56eedea376998548131cdc8a410ebf5e
    Running tests: 446 423
    Checkout 251886b0b192114894e32d90bac59be7b994d59c
    Running tests: 447 424
    Checkout 9fcf08f10cef66e97ee601df493b6498580e05b6


When the benchmark run is done done output will be compressed to a file named
logN-jaxen.{TIMESTAMP}.gz.  This is a gzipped log of all the measurements from
the benchmark run. The log file names follow a similar convention to the test
script names with the E/N suffix.  The raw log file can be found in
{project}/log.tmp during the benchmark run.  The raw log file is very large
and contains output from the test selection tool. For each measured commit the
following information is logged:

* The commit Git hash ("Checkout d44930aa...").
* The javac compile command used to build the system under test.
* The measured javac compile time.
* The modifications to the project files detected by the test selection tool
  ("File added: ...").
* The measured modification checking time.
* The number of Java files the test selection tool parsed.
* The measured test selection tool Java parsing time.
* The measured dependency refreshing/updating time.
* The JUnit tests selected by the test selection tool.
* Total number of tests selected and % of all tests.
* The measured test selection time.
* The JUnit version used to run this test run.
* The test running error output (if any).
* The measured test running time.

For each commit the tests are run twice: once using regression test selection,
and once running all tests (without RTS). The results of the first commit are
usually discarded.

To get some statistics out of the log file there are several R scripts in the
benchmark root directory. To generate the graphs you should first update the
files.R file, which configures the data sources for the other R scripts. Then
run for example

    Rscript makegraphs.R


to generate the graphs for the configured input data. Don't worry if you get
errors, because the script tries to copy the graphs to a sibling directory
which you likely don't have. SVG files for each graph should now appear in the
same directory as the input data file (.gz).


## 4. Using the regression test selection tool manually

It is possible to run the test selection tool manually. First, a configuration
file must be created for your project. The configuration file should be named
dependencies.cfg and contain the following information (update to fit your
project):

    classpath.src=src/java/main\:src/java/test\:src/gen
	classpath.lib=path/to/lib.jar\:path/to/otherlib.jar
	classpath.con=
	java.home=/usr/lib/jvm/jdk1.8.0_71
	bootclasspath=/usr/lib/jvm/jdk1.8.0_71/lib/rt.jar
	testRunner=JUNIT4


(Note that colons must be escaped with \: )

Then using a terminal emulator with the working directory set to the directory
where your dependencies.cfg file is, assuming the path to the AutoRTS Jar file
(launcher.jar, see section 2) is $TOOL, run the following commands:

    java -DlogLevel=INFO -jar $TOOL compile -project .
    java -DlogLevel=INFO -jar $TOOL runtests -project .

This will first compile the project, and then run test selection and
automatically run the selected JUnit tests.
