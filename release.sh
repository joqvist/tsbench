#!/bin/bash

set -eu

# Build a Zip file for release.

git archive --format=zip --prefix=autorts/ HEAD > git.zip
pushd autorts
git archive --format=zip --prefix=autorts/autorts/ HEAD > git.zip
pushd core/extendj
git archive --format=zip --prefix=autorts/autorts/core/extendj/ HEAD > git.zip
popd
popd

if [ -e "autorts.zip" ]; then
	rm "autorts.zip"
fi

zipmerge autorts.zip git.zip autorts/git.zip autorts/core/extendj/git.zip
rm git.zip autorts/git.zip autorts/core/extendj/git.zip
