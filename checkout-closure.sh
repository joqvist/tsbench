#!/bin/bash

set -eu

if [ $# -lt "1" ]; then
    echo "Usage: checkout GITHASH"
    exit 1
fi

if [ ! -d "$PROJECT" ]; then
	echo "Error: no such directory: $PROJECT"
	echo "Make sure that the clean-project script is run first!"
	exit 1
fi

cd "$PROJECT"
echo "Checkout $1" | tee -a log.tmp
git reset --hard $1 > /dev/null
if [ "$?" -ne "0" ]; then
	exit $?
fi

MVNLIB='mvnlibs'
LOCALLIB='lib'
if [ -f "pom.xml" ]; then
	if [ -d "$MVNLIB" ]; then
		rm -r "$MVNLIB"
	fi
	# Download maven dependencies.
	mkdir "${MVNLIB}"
	mvn org.apache.maven.plugins:maven-dependency-plugin:2.1:copy-dependencies -DoutputDirectory="$(readlink -f ${MVNLIB})" >/dev/null

	# Remove libs that cause build problems.
	rm -f "${MVNLIB}/guava-20.0-SNAPSHOT.jar"
	rm -f "${MVNLIB}/guava-gwt-20.0-SNAPSHOT.jar"

	# Build classpath.
	CLASSPATH=$(JARS=("$MVNLIB"/*.jar); IFS=:; echo "${JARS[*]}")
elif [ -d "$LOCALLIB" ]; then
	ant jar >/dev/null
	CLASSPATH=$(JARS=("$LOCALLIB"/*.jar); IFS=:; echo "${JARS[*]}")
	CLASSPATH=$CLASSPATH:$(JARS=("build/lib"/*.jar); IFS=:; echo "${JARS[*]}")
else
	echo "Error: can not find dependencies!"
	exit 1
fi

if [ -d "$MVNLIB" ]; then
	# Run maven build.
	# Generate sources:
	mvn -f pom-main.xml generate-sources >/dev/null
fi

# JUnit version to use.
JUNIT="JUNIT4"

# Build test selector configuration:
echo "classpath.src=src\:test\:gen" > dependencies.cfg
echo "classpath.lib=${CLASSPATH//:/\\:}" >> dependencies.cfg
echo "classpath.con=" >> dependencies.cfg
# New excludes when using latest commits:
#echo "exclude=.+/debugger/.+,.+/gwt/.+,.+/webservice/.+" >> dependencies.cfg
# Old excludes, when using 2014 commits:
echo "exclude=.+/com/google/javascript/jscomp/ant/.+,.+/gwt/.+,.+/webservice/.+" >> dependencies.cfg
echo "java.home=${TEST_JAVA_HOME}" >> dependencies.cfg
echo "bootclasspath=${TEST_JAVA_HOME}/lib/rt.jar" >> dependencies.cfg
echo "testRunner=$JUNIT" >> dependencies.cfg

