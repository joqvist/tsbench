#!/bin/bash

# Update these variables to point to your local Java installations:

JAVA_SE4_HOME="$HOME/jdk/j2sdk1.4.2_19/jre"
JAVA_SE6_HOME="/usr/lib/jvm/jdk1.6.0_45"
JAVA_SE7_HOME="/usr/lib/jvm/jdk1.7.0_45"
#JAVA_SE8_HOME="/usr/lib/jvm/jdk1.8.0_31"
JAVA_SE8_HOME="/usr/lib/jvm/jdk1.8.0_71"


# Example configuration for OSX:

#JAVA_SE6_HOME="/System/Library/Java/JavaVirtualMachines/1.6.0.jdk/Contents/Home"
#JAVA_SE7_HOME="/Library/Java/JavaVirtualMachines/jdk1.7.0_51.jdk/Contents/Home"
#JAVA_SE8_HOME="/Library/Java/JavaVirtualMachines/jdk1.8.0_66.jdk/Contents/Home"
#JAVA_SE8_HOME="/Library/Java/JavaVirtualMachines/jdk1.8.0_73.jdk/Contents/Home"

# Other Java JDKs can be downloaded from the Oracle Java Archive:
# http://www.oracle.com/technetwork/java/javase/archive-139210.html

# Warn if Java installations are missing:

if [ ! -d "$JAVA_SE4_HOME" ]; then
	echo "WARNING: Java 4 installation not found! Java 4 builds will fail."
	echo "WARNING: Please update java-config.sh!!!"
fi
if [ ! -d "$JAVA_SE6_HOME" ]; then
	echo "WARNING: Java 6 installation not found!! Java 6 builds will fail."
	echo "WARNING: Please update java-config.sh!!!"
fi
if [ ! -d "$JAVA_SE7_HOME" ]; then
	echo "WARNING: Java 7 installation not found!! Java 7 builds will fail."
	echo "WARNING: Please update java-config.sh!!!"
fi
if [ ! -d "$JAVA_SE8_HOME" ]; then
	echo "WARNING: Java 8 installation not found!! Java 8 builds will fail."
	echo "WARNING: Please update java-config.sh!!!"
fi
