set -eu

# Run last compile command for project.

if [ $# -lt "1" ]; then
    echo "Usage: $0 PROJECT"
    exit 1
fi

PROJECT=$1
cd "${PROJECT}"

grep "^compile command:" log.tmp | tail -n1 | cut -d ' ' -f3- > lastcompile.sh
chmod u+x lastcompile.sh
./lastcompile.sh
