#!/bin/bash

interrupted()
{
    exit $?
}

trap interrupted SIGINT
set -e

git rev-list --reverse --all > revlist
while read -u 5 rev; do
	echo "Press Enter to checkout $rev"
	read ignore
	git reset --hard $rev
done 5< revlist
