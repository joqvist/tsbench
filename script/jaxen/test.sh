#!/bin/bash

interrupted()
{
    exit $?
}

trap interrupted SIGINT
set -e

if [ ! -d "testbin" ]; then
	mkdir "testbin"
fi

if [ ! -d "lib" ]; then
	mkdir "lib"
	java -jar ivy-2.3.0.jar -dependency jaxen jaxen 1.1.6 -retrieve "lib/[artifact]-[revision](-[classifier]).[ext]"
	java -jar ivy-2.3.0.jar -dependency junit junit 3.8.2 -retrieve "lib/[artifact]-[revision](-[classifier]).[ext]"
	rm lib/jaxen.*
fi

if [ ! -e "sources" ]; then
	find "src/java/main" -name '*.java' > sources
	find "src/java/test" -name '*.java' >> sources
fi

CLASSPATH="lib/dom4j-1.6.1.jar:lib/icu4j-2.6.1.jar:lib/jdom-1.0.jar:lib/junit-3.8.2.jar:lib/xalan-2.6.0.jar:lib/xercesImpl-2.6.2.jar:lib/xml-apis-1.3.02.jar:lib/xmlParserAPIs-2.6.2.jar:lib/xom-1.0.jar" 

javac -d "testbin" -cp $CLASSPATH @sources

java -cp "testbin:$CLASSPATH" junit.textui.TestRunner org.jaxen.test.JaxenTests
